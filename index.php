<!DOCTYPE html>
<html>

<head>
    <title>NETA Initial Task</title>
    <script src="js/jquery-3.5.1.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>

    <div class="container mt-5">
        <div class="card">
            <h5 class="card-header">Secret Key</h5>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        The secret key is: 
                        <span class="secret-key font-weight-bold">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </span>
                    </div>
                    <div class="col-sm-12">
                        The secret key base64 encoded: 
                        <span class="secret-key-base64 font-weight-bold">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        const proxyurl = "https://cors-anywhere.herokuapp.com/", // Using proxy to avoid “No Access-Control-Allow-Origin header” problem
              url      = "https://join.dev.neta.sh/api/interview-tests/vault-of-sweets",        
              tags     = ["a","abbr","acronym","address","applet","area","article","aside","audio","b","base","basefont","bd","bdo",
                "bgsound","big","blink","blockquote","body","br","button","canvas","caption","center","cite","code","col","colgroup",
                "content","data","datalist","dd","decorator","del","details","dfn","dir","div","dl","dt","element","em","embed",
                "fieldset","figcaption","figure","font","footer","form","frame","frameset","h1","h2","h3","h4","h5","h6","head",
                "header","hgroup","hr","html","i","iframe","img","input","ins","isindex","kbd","keygen","label","legend","li","link",
                "listing","main","map","mark","marquee","menu","menuitem","meta","meter","nav","nobr","noframes","noscript","object",
                "ol","optgroup","option","output","p","param","plaintext","pre","progress","q","rp","rt","ruby","s","samp","script",
                "section","select","shadow","small","source","spacer","span","strike","strong","style","sub","summary","sup","svg",
                "table","tbody","td","template","textarea","tfoot","th","thead","time","title","tr","track","tt","u","ul","var",
                "video","wbr","xmp"];
        
        let settings = {
            "url": proxyurl + url,
            "method": "GET",
            "headers": {
                "Candidate-Email": "shuvro_hasan@hotmail.com"
            }
        };

        $.ajax(settings).done(function (data) {
            let $neta = $(data),
                str = '';

            $.each(tags, function(i, val){
                if($(val, $neta).length) {
                    str += val + $(val, $neta).length;
                }    
            });

            $('.secret-key').html(str);
            $('.secret-key-base64').html(btoa(str));
        });
    </script>

</body>

</html>